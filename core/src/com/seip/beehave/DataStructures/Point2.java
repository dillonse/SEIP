package com.seip.beehave.DataStructures;
import java.lang.Math;

/**
 * Created by dillonse on 08/11/2016.
 */

public class Point2 {
    public float x;
    public float y;
    public Point2(float x,float y){
        this.x=x;this.y=y;
    }

    public Point2(Point2 point){
        this(point.x,point.y);
    }
}
