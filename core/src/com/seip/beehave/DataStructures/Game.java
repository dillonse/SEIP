package com.seip.beehave.DataStructures;

import com.seip.beehave.Logic.GameplayInput;
import com.seip.beehave.Logic.GameplayManager;
import com.seip.beehave.Logic.GameplayMode;
import com.seip.beehave.Logic.InputAdaptor;
import com.seip.beehave.UserInterface.Dialogs.LoseDialog;
import com.seip.beehave.UserInterface.Dialogs.WinDialog;

/**
 * Created by dillonse on 22/11/2016.
 */

public class Game {
    private static Game instance;
    public static Game getInstance(){
        if(instance==null)instance= new Game();
        return instance;
    }
    private Game(){}

    private HexagonGrid grid;
    private Overbar overbar;
    private GameplayManager gameplayManager;
    private GameplayMode gameplayMode;
    private GameplayInput gameplayInput;
    private LoseDialog loseDialog;

    private WinDialog winDialog;

    private InputAdaptor inputAdaptor;

    public Overbar getOverbar() {
        return overbar;
    }

    public void setOverbar(Overbar overbar) {
        this.overbar = overbar;
    }

    public HexagonGrid getGrid() {
        return grid;
    }

    public void setGrid(HexagonGrid grid) {
        this.grid = grid;
    }

    public GameplayInput getGameplayInput() {
        return gameplayInput;
    }

    public void setGameplayInput(GameplayInput gameplayInput) {
        this.gameplayInput = gameplayInput;
    }

    public GameplayMode getGameplayMode() {
        return gameplayMode;
    }

    public void setGameplayMode(GameplayMode gameplayMode) {
        this.gameplayMode = gameplayMode;
    }

    public GameplayManager getGameplayManager() {
        return gameplayManager;
    }

    public void setGameplayManager(GameplayManager gameplayManager) {
        this.gameplayManager = gameplayManager;
    }

    public LoseDialog getLoseDialog() {
        return loseDialog;
    }

    public void setLoseDialog(LoseDialog loseDialog) {
        this.loseDialog = loseDialog;
    }

    public WinDialog getWinDialog() {
        return winDialog;
    }

    public void setWinDialog(WinDialog winDialog) {
        this.winDialog = winDialog;
    }


    public InputAdaptor getInputAdaptor() {
        return inputAdaptor;
    }

    public void setInputAdaptor(InputAdaptor inputAdaptor) {
        this.inputAdaptor = inputAdaptor;
    }
}
