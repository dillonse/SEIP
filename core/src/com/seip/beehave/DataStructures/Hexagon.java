package com.seip.beehave.DataStructures;

import com.badlogic.gdx.graphics.Color;
import com.seip.beehave.Rendering.HexagonRenderer;


/**
 * Created by dillonse on 08/11/2016.
 * This class contains the information that each hexagon of the grid is associated with.
 */


public class Hexagon {
    private int rank;
    private HexagonType type;
    private HexagonRenderer renderer;

    public void initRenderer(Point2 position, Point2 scale){
        renderer = new HexagonRenderer(position,scale);
    }

    public void renderColour(){
        renderer.renderColour();
    }

    public void renderTexture(){
        renderer.renderTexture();
    }

    public Point2 getCentre(){
        return renderer.getCentre();
    }

    public void setColor(Color color){
       renderer.setColor(color);
    }

    public void setRenderTexture(boolean renderTexture){
        renderer.setRenderTexture(renderTexture);
    }
    //Accessors
    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public HexagonType getType() {
        return type;
    }

    public void setType(HexagonType type) {
        this.type = type;
    }

}

