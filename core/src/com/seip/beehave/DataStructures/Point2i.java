package com.seip.beehave.DataStructures;

/**
 * Created by dillonse on 21/11/2016.
 */

public class Point2i {
        public int x;
        public int y;
        public Point2i(int x,int y){
            this.x=x;this.y=y;
        }
        public Point2i(Point2i point){
            this(point.x,point.y);
        }
}
