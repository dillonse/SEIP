package com.seip.beehave.DataStructures;

import com.seip.beehave.UserInterface.Screens.GameplayScreen;
import com.seip.beehave.UserInterface.Screens.LevelsScreen;
import com.seip.beehave.UserInterface.Screens.LevelsTierScreen;
import com.seip.beehave.UserInterface.Screens.MainMenuScreen;
import com.seip.beehave.UserInterface.Screens.TutorialScreen;

/**
 * Created by dillonse on 30/11/2016.
 */

public class Screens {
    public static GameplayScreen gameplayScreen;
    public static MainMenuScreen mainMenuScreen;
    public static LevelsScreen levelsScreenEasy;
    public static LevelsScreen levelsScreenMedium;
    public static LevelsScreen levelsScreenHard;
    public static LevelsTierScreen levelsTierScreen;
    public static TutorialScreen tutorialScreen;
}
