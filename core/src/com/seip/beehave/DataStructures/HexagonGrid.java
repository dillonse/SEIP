package com.seip.beehave.DataStructures;

import com.badlogic.gdx.Gdx;
import com.seip.beehave.Rendering.HexagonGridRenderer;

import java.lang.Math;
/**
 * Created by dillonse on 08/11/2016.
 */

public class HexagonGrid {
    Hexagon[][] grid;
    HexagonGridRenderer renderer;

    public HexagonGrid(){};

    public HexagonGrid(int sizeX, int sizeY){
        sizeX+=2;sizeY+=2;
        grid = new Hexagon[sizeX][sizeY];
        for (int i=0;i<grid.length;i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = new Hexagon();
                grid[i][j].setType(HexagonType.Vacant);
            }
        }
        renderer= new HexagonGridRenderer(grid);
    }

    public void render(){
        renderer.renderColours(grid);
        renderer.renderOutline(grid);
    }

    public int getNumOfCells(){
        return grid.length*grid[grid.length-1].length;
    }

    public Hexagon locate(Point2 query){
        //convert query point to cell space
        int bestX = (int)Math.abs(Math.floor(((query.x-0.25*renderer.getScaleX())/Gdx.graphics.getWidth())*grid.length));
        if(bestX%2==0)
            query.y+=0.5*renderer.getScaleY();
        int bestY = (int)Math.abs(Math.floor((query.y-0.25*renderer.getScaleY())/Gdx.graphics.getHeight()*grid[grid.length-1].length));
        //check that the point is inside of the hex
        if(bestX>=grid.length||bestY>=grid[grid.length-1].length)return grid[0][0];
        return grid[bestX][bestY];
    }

    public Hexagon getHexagon(int i,int j){
        return grid[i][j];
    }
}
