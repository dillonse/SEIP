package com.seip.beehave.DataStructures;

/**
 * Created by dillonse on 21/11/2016.
 */

public enum HexagonType
{
    Vacant,Occupied,ExploredVacant,ExploredOccupied,Border
}