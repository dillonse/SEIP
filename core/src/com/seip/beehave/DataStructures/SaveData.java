package com.seip.beehave.DataStructures;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Created by dillonse on 30/11/2016.
 */

public class SaveData {
    public static int tier;
    public static int level;
    public static void save(){
        Preferences prefs = Gdx.app.getPreferences("Player");
        prefs.putInteger("level",level);
        prefs.putInteger("tier",tier);
        prefs.flush();
    }

    public static void load(){
        Preferences prefs = Gdx.app.getPreferences("Player");
        level= prefs.getInteger("level",1);
        tier = prefs.getInteger("tier",1);
    }
}
