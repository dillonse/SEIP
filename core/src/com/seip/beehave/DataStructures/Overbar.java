package com.seip.beehave.DataStructures;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Timer;
import com.seip.beehave.Rendering.OverbarRenderer;

import java.sql.Time;

/**
 * Created by dillonse on 09/11/2016.
 * This class contains all information/functionalities associated with the overbar such as the timer, the score etc
 */

public class Overbar {
    OverbarRenderer renderer;
    Time time;
    int kills;

    public Overbar(){
        time= new Time(0);
        renderer = new OverbarRenderer();
        renderer.setTimeString("0:00");
        renderer.setKillsString("0");
    }

    public void render(){
        String timerString = time.toString().substring(3,8);
        renderer.setTimeString(timerString);
        renderer.setKillsString("x "+(Integer.toString(kills)));
        renderer.render();
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {

        this.time = time;
    }
    public void setKills(int kills) {
        this.kills = kills;
    }
}
