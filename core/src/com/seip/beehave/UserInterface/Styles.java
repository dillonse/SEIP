package com.seip.beehave.UserInterface;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.seip.beehave.DataStructures.Point2;

/**
 * Created by dillonse on 29/11/2016.
 */

public class Styles {
    static Skin selection;

    static Point2  scale=new Point2(Gdx.graphics.getHeight()/1440f, Gdx.graphics.getWidth()/2560f);
    public static Skin getDescriptionButtonStyle(){
        //scale=new Point2(Gdx.graphics.getHeight()/1440f, Gdx.graphics.getWidth()/2560f);

        Skin skin= Fonts.Lobster.getSkin(new Float(80*scale.x).intValue(), Colors.fontColor);

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.down = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.checked = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.over = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.font = skin.getFont("default");
        skin.add("default", textButtonStyle);
        return skin;
    }

    public static Skin getSelectionButtonStyle(){
       // scale = new
        Skin skin= Fonts.Lobster.getSkin(new Float(124*scale.x).intValue(), Colors.fontColor);

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.down = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.checked = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.over = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.font = skin.getFont("default");
        skin.add("default", textButtonStyle);
        return skin;

    }

    public static Skin getUnavailableSelectionButtonStyle(){
        Skin skin= Fonts.Lobster.getSkin(new Float(124*scale.x).intValue(), Colors.fontColor);

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.down = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.checked = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.over = skin.newDrawable("default", Colors.menuOptionsColor);
        textButtonStyle.font = skin.getFont("default");
        textButtonStyle.fontColor = Colors.unavailableFontColor;
        skin.add("default", textButtonStyle);
        return skin;
    }

    public static Skin getHeaderButtonStyle(){
        Skin skin = Fonts.Lobster.getSkin(new Float(150*scale.x).intValue(),Colors.fontColor);

        TextButton.TextButtonStyle bigTextButtonStyle = new TextButton.TextButtonStyle();
        bigTextButtonStyle.up = skin.newDrawable("default", Colors.menuHeaderColor);
        bigTextButtonStyle.down = skin.newDrawable("default", Colors.menuHeaderColor);
        bigTextButtonStyle.checked = skin.newDrawable("default", Colors.menuHeaderColor);
        bigTextButtonStyle.over = skin.newDrawable("default", Colors.menuHeaderColor);
        bigTextButtonStyle.font = skin.getFont("default");
        skin.add("default", bigTextButtonStyle);
        return skin;
    }

    public static Point2 getScale() {
        return scale;
    }

}
