package com.seip.beehave.UserInterface.Dialogs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.seip.beehave.BeehaveGame;
import com.seip.beehave.DataStructures.Game;
import com.seip.beehave.DataStructures.Screens;
import com.seip.beehave.Rendering.DialogRenderer;
import com.seip.beehave.UserInterface.Screens.MainMenuScreen;

/**
 * Created by dillonse on 22/11/2016.
 */

public class LoseDialog {
    private Stage stage;
    private DialogRenderer renderer;
    private boolean isEnabled;
    private BeehaveGame game;

    public LoseDialog(BeehaveGame game) {
        this.game= game;
        stage = new Stage();
        renderer = new DialogRenderer("Exit","Retry",stage);
        renderer.getLeft().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleExit();
            }
        });
        renderer.getRight().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Game.getInstance().getGameplayManager().resetGame();
                Game.getInstance().getLoseDialog().setEnabled(false);
            }
        });
    }

    public void render(){
        if(isEnabled)
            renderer.render(stage);
    }

    private void handleExit(){
        game.setScreen(Screens.mainMenuScreen);
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
