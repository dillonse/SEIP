package com.seip.beehave.UserInterface.Dialogs;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.seip.beehave.BeehaveGame;
import com.seip.beehave.DataStructures.Game;
import com.seip.beehave.DataStructures.SaveData;
import com.seip.beehave.DataStructures.Screens;
import com.seip.beehave.Logic.GameplayMode;
import com.seip.beehave.Rendering.DialogRenderer;
import com.seip.beehave.UserInterface.Screens.GameplayScreen;
import com.seip.beehave.UserInterface.Screens.MainMenuScreen;

/**
 * Created by dillonse on 22/11/2016.
 */

public class WinDialog {
    private Stage stage;
    private DialogRenderer renderer;
    private boolean isEnabled;
    private BeehaveGame game;
    public WinDialog(BeehaveGame game) {
        this.game=game;
        stage = new Stage();
        renderer = new DialogRenderer("Exit","Continue",stage);
        renderer.getLeft().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleExit();
            }
        });
        renderer.getRight().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleContinue();
            }
        });
    }

    public void render(){
        if(isEnabled)
            renderer.render(stage);
    }

    private void handleExit(){
        game.setScreen(new MainMenuScreen(game));
    }

    private void handleContinue() {
        GameplayMode mode= Game.getInstance().getGameplayMode();
        if(mode.getLevel()==9&& mode.getTier()==3){
            //@TODO congrats screen
        }
        else {
            if (mode.getLevel() == 9) {
                mode.setLevel(0);
                mode.setTier(mode.getTier()+1);
            } else {
                mode.setLevel(mode.getLevel()+1);
            }
            Screens.gameplayScreen = new GameplayScreen(game,mode.getTier(),mode.getLevel());
            game.setScreen(Screens.gameplayScreen);
        }
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
