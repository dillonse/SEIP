package com.seip.beehave.UserInterface;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by dillonse on 08/11/2016.
 */

public class Colors {
    public static Color edgeColor = new Color(130.0f/255.0f, 66f/255f, 3f/255f, 1);
    public static Color unexploredColor = new Color( 226f / 255f,138f / 255f,2f / 255f,1);
    public static Color exploredColor = new Color(163f/255f,131f/255f,80f/255f,1);
    public static Color outlineColor = new Color(1,1,1,1);
    public static Color overbarColor = new Color(0,128f/255f,127f/255f,1);
    public static Color exploredWarningFarColor = new Color(233f/255f,103f/255f,0,1);
    public static Color exploredWarningNearColor = new Color(240f/255f,69f/255f,0,1);
    public static Color messageColor = new Color(0,128f/255f,127f/255f,0.75f);
    public static Color fontColor = new Color(1,1,1,1);
    public static Color unavailableFontColor = new Color(0.75f,0.75f,0.75f,1f);
    public static Color menuOptionsColor = new Color( 226f / 255f,138f / 255f,2f / 255f,1f);
    public static Color menuHeaderColor = new Color(0,128f/255f,127f/255f,1);
}
