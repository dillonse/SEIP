package com.seip.beehave.UserInterface;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by dillonse on 15/11/2016.
 */

public class Textures {
    private static Textures instance;
    public static Textures getInstance(){
        if(instance==null)instance = new Textures();
        return instance;
    }
    public  Texture casualBee = new Texture(Gdx.files.internal("img/casual.png"));
    public  Texture exploredHexagon = new Texture(Gdx.files.internal("img/explored.png"));
    public  Texture exploredNearHex = new Texture(Gdx.files.internal("img/near.png"));
    public  Texture exploredFarHex = new Texture(Gdx.files.internal("img/far.png"));
    public  Texture exploredKillHex = new Texture(Gdx.files.internal("img/kill.png"));
    public  Texture killCounter = new Texture(Gdx.files.internal("img/kills.png"));
    public  Texture timeCounter = new Texture(Gdx.files.internal("img/time.png"));
    public  Texture unexploredHex = new Texture(Gdx.files.internal("img/unexplored.png"));
}
