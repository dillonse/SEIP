package com.seip.beehave.UserInterface.Screens;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.GL20;
import com.seip.beehave.BeehaveGame;
import com.seip.beehave.DataStructures.*;
import com.seip.beehave.DataStructures.Game;
import com.seip.beehave.Logic.GameplayInput;
import com.seip.beehave.Logic.GameplayManager;
import com.seip.beehave.Logic.GameplayMode;
import com.seip.beehave.Logic.InputAdaptor;
import com.seip.beehave.Testing.ApplicationInputBenchmark;
import com.seip.beehave.UserInterface.Colors;
import com.seip.beehave.DataStructures.Overbar;
import com.seip.beehave.UserInterface.Dialogs.LoseDialog;
import com.seip.beehave.UserInterface.Dialogs.WinDialog;

public class GameplayScreen implements Screen{

	BeehaveGame beehaveGame;
	Game game;
	Point2i dimensions;
	ApplicationInputBenchmark benchmark= new ApplicationInputBenchmark();
	public GameplayScreen(BeehaveGame behaveGame,int tier,int level){

		this.beehaveGame = behaveGame;
		game = Game.getInstance();
		game.setGameplayMode(new GameplayMode(tier,level));
		dimensions = game.getGameplayMode().getGridDimensions();
		game.setGrid(new HexagonGrid(dimensions.x,dimensions.y));
		game.setOverbar(new Overbar());
		game.setGameplayManager(new GameplayManager());
		game.setGameplayInput(new GameplayInput());
		game.getGameplayManager().placeBees();

		game.setLoseDialog(new LoseDialog(beehaveGame));
		game.setWinDialog(new WinDialog(behaveGame));
		game.setInputAdaptor(new InputAdaptor());
	}

	public void render (float f) {

		Gdx.gl.glClearColor(Colors.edgeColor.r,Colors.edgeColor.g,Colors.edgeColor.b,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.getGrid().render();
		game.getOverbar().render();
		game.getLoseDialog().render();
		game.getWinDialog().render();

	}

	public void show(){
		Gdx.input.setInputProcessor(game.getInputAdaptor());
		game.getGameplayManager().resetGame();
	}

	public void hide(){
	}

	public void dispose () {

	}

	public void pause(){
		;
	}

	public void resume(){

	}

	public void resize(int i,int j){

	}

}
