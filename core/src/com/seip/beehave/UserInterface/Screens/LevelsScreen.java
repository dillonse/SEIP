package com.seip.beehave.UserInterface.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.seip.beehave.BeehaveGame;
import com.seip.beehave.DataStructures.SaveData;
import com.seip.beehave.DataStructures.Screens;
import com.seip.beehave.UserInterface.Colors;
import com.seip.beehave.UserInterface.Styles;

/**
 * Created by dillonse on 29/11/2016.
 */

public class LevelsScreen implements Screen {
    BeehaveGame game;
    String name;
    int tier;
    int level;

    private TextButton oneButton;
    private TextButton twoButton;
    private TextButton threeButton;
    private TextButton fourButton;
    private TextButton fiveButton;
    private TextButton sixButton;
    private TextButton sevenButton;
    private TextButton eightButton;
    private TextButton nineButton;
    private TextButton backButton;

    private TextButton levelButton;

    private Stage stage;
    private ShapeRenderer renderer;

    private float verticalOffset;
    private float horizontalOffset;
    public LevelsScreen(BeehaveGame game,String name){
        this.game=game;
        this.name=name;
        if(name=="Easy")tier=1;
        else if(name=="Medium")tier=2;
        else if(name=="Hard")tier=3;
        refresh();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Colors.menuHeaderColor.r,Colors.menuHeaderColor.g,Colors.menuHeaderColor.b,Colors.menuHeaderColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.rect(2*verticalOffset,0,Gdx.graphics.getWidth()-2*verticalOffset,Gdx.graphics.getHeight());
        renderer.end();
        stage.draw();

    }

    void handleBack(){
        game.setScreen(Screens.levelsTierScreen);
    }

    void handleLevel(){
        if(SaveData.tier*10+SaveData.level<tier*10+level)return;
        Screens.gameplayScreen = new GameplayScreen(game,tier,level);
        game.setScreen(Screens.gameplayScreen);
    }

    public void refresh(){
        stage = new Stage();
        verticalOffset= Gdx.graphics.getWidth()/11.5f;
        horizontalOffset = Gdx.graphics.getHeight()/13;
        Skin selectionSkin = Styles.getSelectionButtonStyle();
        Skin headerSkin = Styles.getHeaderButtonStyle();
        Skin unavailableSkin= Styles.getUnavailableSelectionButtonStyle();

        int playerRank= SaveData.tier*10+SaveData.level;
        oneButton= new TextButton("1",playerRank>=tier*10+1?selectionSkin:unavailableSkin,"default");
        oneButton.setTransform(true);
        oneButton.rotateBy(90);
        oneButton.setPosition(verticalOffset*4f,horizontalOffset*2.5f-oneButton.getWidth()*0.5f);

        twoButton= new TextButton("2",playerRank>=tier*10+2?selectionSkin:unavailableSkin,"default");
        twoButton.setTransform(true);
        twoButton.rotateBy(90);
        twoButton.setPosition(verticalOffset*4f,horizontalOffset*6.5f-twoButton.getWidth()*0.5f);

        threeButton= new TextButton("3",playerRank>=tier*10+3?selectionSkin:unavailableSkin,"default");
        threeButton.setTransform(true);
        threeButton.rotateBy(90);
        threeButton.setPosition(verticalOffset*4f,horizontalOffset*10.5f-threeButton.getWidth()*0.5f);

        fourButton= new TextButton("4",playerRank>=tier*10+4?selectionSkin:unavailableSkin,"default");
        fourButton.setTransform(true);
        fourButton.rotateBy(90);
        fourButton.setPosition(verticalOffset*6.5f,horizontalOffset*2.5f-fourButton.getWidth()*0.5f);

        fiveButton= new TextButton("5",playerRank>=tier*10+5?selectionSkin:unavailableSkin,"default");
        fiveButton.setTransform(true);
        fiveButton.rotateBy(90);
        fiveButton.setPosition(verticalOffset*6.5f,horizontalOffset*6.5f-fiveButton.getWidth()*0.5f);

        sixButton= new TextButton("6",playerRank>=tier*10+6?selectionSkin:unavailableSkin,"default");
        sixButton.setTransform(true);
        sixButton.rotateBy(90);
        sixButton.setPosition(verticalOffset*6.5f,horizontalOffset*10.5f-sixButton.getWidth()*0.5f);

        sevenButton= new TextButton("7",playerRank>=tier*10+7?selectionSkin:unavailableSkin,"default");
        sevenButton.setTransform(true);
        sevenButton.rotateBy(90);
        sevenButton.setPosition(verticalOffset*9f,horizontalOffset*2.5f-sevenButton.getWidth()*0.5f);

        eightButton= new TextButton("8",playerRank>=tier*10+8?selectionSkin:unavailableSkin,"default");
        eightButton.setTransform(true);
        eightButton.rotateBy(90);
        eightButton.setPosition(verticalOffset*9f,horizontalOffset*6.5f-eightButton.getWidth()*0.5f);

        nineButton= new TextButton("9",playerRank>=tier*10+9?selectionSkin:unavailableSkin,"default");
        nineButton.setTransform(true);
        nineButton.rotateBy(90);
        nineButton.setPosition(verticalOffset*9f,horizontalOffset*10.5f-nineButton.getWidth()*0.5f);

        backButton = new TextButton("back",selectionSkin,"default");
        backButton.setTransform(true);
        backButton.rotateBy(90);
        backButton.setPosition(verticalOffset*11.25f,Gdx.graphics.getHeight()*0.5f-backButton.getWidth()*0.5f);

        levelButton = new TextButton(name,headerSkin,"default");
        levelButton.setTransform(true);
        levelButton.rotateBy(90);
        levelButton.setPosition(verticalOffset*1.5f,Gdx.graphics.getHeight()*0.5f-levelButton.getWidth()*0.5f);

        oneButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                level=1;
                handleLevel();
            }
        });

        twoButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                level=2;
                handleLevel();
            }
        });


        threeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                level=3;
                handleLevel();
            }
        });


        fourButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                level=4;
                handleLevel();
            }
        });


        fiveButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                level=5;
                handleLevel();
            }
        });


        sixButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                level=6;
                handleLevel();
            }
        });


        sevenButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                level=7;
                handleLevel();
            }
        });

        eightButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                level=8;
                handleLevel();
            }
        });

        nineButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                level=9;
                handleLevel();
            }
        });


        stage = new Stage();
        stage.addActor(oneButton);
        stage.addActor(twoButton);
        stage.addActor(threeButton);
        stage.addActor(fourButton);
        stage.addActor(fiveButton);
        stage.addActor(sixButton);
        stage.addActor(sevenButton);
        stage.addActor(eightButton);
        stage.addActor(nineButton);
        stage.addActor(backButton);
        stage.addActor(levelButton);
        renderer = new ShapeRenderer();
        renderer.setColor(Colors.menuOptionsColor);

        renderer = new ShapeRenderer();
        renderer.setColor(Colors.menuOptionsColor);

        backButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleBack();
            }
        });

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
