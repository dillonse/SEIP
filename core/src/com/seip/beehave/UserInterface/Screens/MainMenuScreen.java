package com.seip.beehave.UserInterface.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.seip.beehave.BeehaveGame;
import com.seip.beehave.DataStructures.Screens;
import com.seip.beehave.UserInterface.Colors;
import com.seip.beehave.UserInterface.Fonts;
import com.seip.beehave.UserInterface.Styles;

/**
 * Created by dillonse on 29/11/2016.
 */

public class MainMenuScreen implements Screen{

    private BeehaveGame game=null;
    private OrthographicCamera camera;

    private TextButton continueButton;
    private TextButton levelsButton;
    private TextButton tutorialButton;
    private TextButton settingButton;
    private TextButton exitButton;
    private TextButton nameButton;

    private Stage stage;
    private ShapeRenderer renderer;

    private float offset;

    public MainMenuScreen(BeehaveGame game){
        this.game=game;
        camera= new OrthographicCamera();
        camera.setToOrtho(false,800,480);

        Skin selectionSkin= Styles.getSelectionButtonStyle();
        Skin headerSkin = Styles.getHeaderButtonStyle();

        offset= Gdx.graphics.getWidth()/15f;
        continueButton= new TextButton("Continue",selectionSkin,"default");
        continueButton.setTransform(true);
        continueButton.rotateBy(90);
        continueButton.setPosition(offset*4f,Gdx.graphics.getHeight()*0.5f-continueButton.getWidth()*0.5f);

        levelsButton= new TextButton("Levels",selectionSkin,"default");
        levelsButton.setTransform(true);
        levelsButton.rotateBy(90);
        levelsButton.setPosition(offset*6.5f,Gdx.graphics.getHeight()*0.5f-levelsButton.getWidth()*0.5f);

        tutorialButton= new TextButton("Tutorial",selectionSkin,"default");
        tutorialButton.setTransform(true);
        tutorialButton.rotateBy(90);
        tutorialButton.setPosition(offset*9f,Gdx.graphics.getHeight()*0.5f-tutorialButton.getWidth()*0.5f);

        settingButton= new TextButton("Settings",selectionSkin,"default");
        settingButton.setTransform(true);
        settingButton.rotateBy(90);
        settingButton.setPosition(offset*11.5f,Gdx.graphics.getHeight()*0.5f-settingButton.getWidth()*0.5f);

        exitButton= new TextButton("Exit",selectionSkin,"default");
        exitButton.setTransform(true);
        exitButton.rotateBy(90);
        exitButton.setPosition(offset*14f,Gdx.graphics.getHeight()*0.5f-exitButton.getWidth()*0.5f);

        nameButton = new TextButton("Beehave",headerSkin,"default");
        nameButton.setTransform(true);
        nameButton.rotateBy(90);
        nameButton.setPosition(offset*2f,Gdx.graphics.getHeight()*0.5f-nameButton.getWidth()*0.5f);

        continueButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleContinue();
            }
        });

        exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleExit();
            }
        });

        levelsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleLevels();
            }
        });

        tutorialButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleTutorial();
            }
        });

        stage = new Stage();
        stage.addActor(continueButton);
        stage.addActor(levelsButton);
        stage.addActor(tutorialButton);
        stage.addActor(settingButton);
        stage.addActor(exitButton);
        stage.addActor(nameButton);

        renderer = new ShapeRenderer();
        renderer.setColor(Colors.menuOptionsColor);
    }

    @Override
    public void dispose(){
        ;
    }

    public void hide(){
        ;//
    }

    public void show(){

        Gdx.input.setInputProcessor(stage);
    }

    public void render(float delta){
        Gdx.gl.glClearColor(Colors.menuHeaderColor.r,Colors.menuHeaderColor.g,Colors.menuHeaderColor.b,Colors.menuHeaderColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.rect(2*offset,0,Gdx.graphics.getWidth()-2*offset,Gdx.graphics.getHeight());
        renderer.end();
        stage.draw();

    }

    public void pause(){
        ;
    }

    public void resume(){

    }

    public void resize(int i,int j){

    }

    private void handleContinue(){
        game.setScreen(Screens.gameplayScreen);
    }

    private void handleExit(){
        Gdx.app.exit();
    }

    private void handleLevels(){
        game.setScreen(Screens.levelsTierScreen);
    }

    private void handleTutorial(){
        game.setScreen(Screens.tutorialScreen);
    }
    //@TODO add handling for other functionalities
}
