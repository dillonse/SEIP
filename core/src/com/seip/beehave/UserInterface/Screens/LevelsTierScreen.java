package com.seip.beehave.UserInterface.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.seip.beehave.BeehaveGame;
import com.seip.beehave.DataStructures.Screens;
import com.seip.beehave.UserInterface.Colors;
import com.seip.beehave.UserInterface.Styles;

/**
 * Created by dillonse on 29/11/2016.
 */

public class LevelsTierScreen implements Screen{
    private BeehaveGame game=null;

    private TextButton easyButton;
    private TextButton mediumButton;
    private TextButton hardButton;
    private TextButton backButton;
    private TextButton tierButton;

    private Stage stage;
    private ShapeRenderer renderer;

    private float offset;

    public LevelsTierScreen(BeehaveGame game){
        this.game = game;
        offset= Gdx.graphics.getWidth()/11.5f;
        Skin selectionSkin = Styles.getSelectionButtonStyle();
        Skin headerSkin = Styles.getHeaderButtonStyle();

        tierButton= new TextButton("Tier",headerSkin,"default");
        tierButton.setTransform(true);
        tierButton.rotateBy(90);
        tierButton.setPosition(offset*1.5f,Gdx.graphics.getHeight()*0.5f-tierButton.getWidth()*0.5f);

        easyButton= new TextButton("Easy",selectionSkin,"default");
        easyButton.setTransform(true);
        easyButton.rotateBy(90);
        easyButton.setPosition(offset*4f,Gdx.graphics.getHeight()*0.5f-easyButton.getWidth()*0.5f);

        mediumButton= new TextButton("Medium",selectionSkin,"default");
        mediumButton.setTransform(true);
        mediumButton.rotateBy(90);
        mediumButton.setPosition(offset*6.5f,Gdx.graphics.getHeight()*0.5f-mediumButton.getWidth()*0.5f);

        hardButton= new TextButton("Hard",selectionSkin,"default");
        hardButton.setTransform(true);
        hardButton.rotateBy(90);
        hardButton.setPosition(offset*9f,Gdx.graphics.getHeight()*0.5f-hardButton.getWidth()*0.5f);

        backButton= new TextButton("Back",selectionSkin,"default");
        backButton.setTransform(true);
        backButton.rotateBy(90);
        backButton.setPosition(offset*11.25f,Gdx.graphics.getHeight()*0.5f-backButton.getWidth()*0.5f);

        stage = new Stage();
        stage.addActor(easyButton);
        stage.addActor(mediumButton);
        stage.addActor(hardButton);
        stage.addActor(backButton);
        stage.addActor(tierButton);
        renderer = new ShapeRenderer();
        renderer.setColor(Colors.menuOptionsColor);

        backButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleBack();
            }
        });

        easyButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleEasy();
            }
        });

        mediumButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleMedium();
            }
        });

        hardButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleHard();
            }
        });


    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Colors.menuHeaderColor.r,Colors.menuHeaderColor.g,Colors.menuHeaderColor.b,Colors.menuHeaderColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.rect(2*offset,0,Gdx.graphics.getWidth()-2*offset,Gdx.graphics.getHeight());
        renderer.end();
        stage.draw();
    }

    void handleBack(){
        game.setScreen(Screens.mainMenuScreen);
    }

    void handleEasy(){
        game.setScreen(Screens.levelsScreenEasy);
    }

    void handleMedium(){
        game.setScreen(Screens.levelsScreenMedium);
    }

    void handleHard(){
        game.setScreen(Screens.levelsScreenHard);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
