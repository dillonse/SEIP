package com.seip.beehave.UserInterface.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.seip.beehave.BeehaveGame;
import com.seip.beehave.DataStructures.Screens;
import com.seip.beehave.UserInterface.Colors;
import com.seip.beehave.UserInterface.Styles;
import com.seip.beehave.UserInterface.Textures;

/**
 * Created by dillonse on 30/11/2016.
 */

public class TutorialScreen implements Screen{
    Stage stage;
    SpriteBatch batch= new SpriteBatch();
    BeehaveGame game;
    float offset;
    ShapeRenderer renderer = new ShapeRenderer();
    private float iconWidth=100*Styles.getScale().x;
    private TextButton tutorialButton;
    private TextButton unexploredButton;
    private TextButton exploredButton;
    private TextButton exploredFarButton;
    private TextButton exploredNearButton;
    private TextButton exploredBeeButton;
    private TextButton killsLimitButton;
    private TextButton timeLimitButton;
    private TextButton backButton;
    public TutorialScreen(BeehaveGame game){
        this.game = game;
        offset = Gdx.graphics.getWidth()/22f;

        Skin descriptionSkin= Styles.getDescriptionButtonStyle();
        Skin headerSkin = Styles.getHeaderButtonStyle();
        Skin selectionSkin = Styles.getSelectionButtonStyle();

        tutorialButton= new TextButton("Tutorial",headerSkin,"default");
        tutorialButton.setTransform(true);
        tutorialButton.rotateBy(90);
        tutorialButton.setPosition(offset*2f,Gdx.graphics.getHeight()*0.5f-tutorialButton.getWidth()*0.5f);

        unexploredButton= new TextButton("this is honey! Yum!",descriptionSkin,"default");
        unexploredButton.setTransform(true);
        unexploredButton.rotateBy(90);
        unexploredButton.setPosition(offset*4.5f,iconWidth*3);

        exploredButton= new TextButton("no bees around! Explore carelessly!",descriptionSkin,"default");
        exploredButton.setTransform(true);
        exploredButton.rotateBy(90);
        exploredButton.setPosition(offset*7f,iconWidth*3);

        exploredFarButton= new TextButton("a bee is close by.. careful now!",descriptionSkin,"default");
        exploredFarButton.setTransform(true);
        exploredFarButton.rotateBy(90);
        exploredFarButton.setPosition(offset*9.5f,iconWidth*3);

        exploredNearButton= new TextButton("many bees are close by.. beehave!",descriptionSkin,"default");
        exploredNearButton.setTransform(true);
        exploredNearButton.rotateBy(90);
        exploredNearButton.setPosition(offset*12f,iconWidth*3);

        exploredBeeButton= new TextButton("you killed a bee! Shame! Shame!",descriptionSkin,"default");
        exploredBeeButton.setTransform(true);
        exploredBeeButton.rotateBy(90);
        exploredBeeButton.setPosition(offset*14.5f,iconWidth*3);


        killsLimitButton= new TextButton("max number of bees you may kill!",descriptionSkin,"default");
        killsLimitButton.setTransform(true);
        killsLimitButton.rotateBy(90);
        killsLimitButton.setPosition(offset*17f,iconWidth*3f);
        timeLimitButton= new TextButton("time until they start chasing you!",descriptionSkin,"default");
        timeLimitButton.setTransform(true);
        timeLimitButton.rotateBy(90);
        timeLimitButton.setPosition(offset*19.5f,iconWidth*3f);

        backButton= new TextButton("Back",selectionSkin,"default");
        backButton.setTransform(true);
        backButton.rotateBy(90);
        backButton.setPosition(offset*21.5f,Gdx.graphics.getHeight()*0.5f-backButton.getWidth()*0.5f);

        backButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleBack();
            }
        });


        stage =new Stage();
        stage.addActor(tutorialButton);
        stage.addActor(unexploredButton);
        stage.addActor(exploredButton);
        stage.addActor(exploredFarButton);
        stage.addActor(exploredNearButton);
        stage.addActor(exploredBeeButton);
        stage.addActor(killsLimitButton);
        stage.addActor(timeLimitButton);
        stage.addActor(backButton);

        renderer.setColor(Colors.menuOptionsColor);

    }

    void handleBack(){
        game.setScreen(Screens.mainMenuScreen);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Colors.menuHeaderColor.r,Colors.menuHeaderColor.g,Colors.menuHeaderColor.b,Colors.menuHeaderColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.rect(2*offset,0,Gdx.graphics.getWidth()-2*offset,Gdx.graphics.getHeight());
        renderer.end();
        stage.draw();
        batch.begin();
        batch.draw(Textures.getInstance().unexploredHex,unexploredButton.getX()-unexploredButton.getHeight()*0.5f-100*0.5f,iconWidth,iconWidth,iconWidth);
        batch.draw(Textures.getInstance().exploredHexagon,exploredButton.getX()-exploredButton.getHeight()*0.5f-100*0.5f,iconWidth,iconWidth,iconWidth);
        batch.draw(Textures.getInstance().exploredFarHex,exploredFarButton.getX()-exploredFarButton.getHeight()*0.5f-100*0.5f,iconWidth,iconWidth,iconWidth);
        batch.draw(Textures.getInstance().exploredNearHex,exploredNearButton.getX()-exploredNearButton.getHeight()*0.5f-100*0.5f,iconWidth,iconWidth,iconWidth);
        batch.draw(Textures.getInstance().exploredKillHex,exploredBeeButton.getX()-exploredBeeButton.getHeight()*0.5f-100*0.5f,iconWidth,iconWidth,iconWidth);
        batch.draw(Textures.getInstance().killCounter,killsLimitButton.getX()-killsLimitButton.getHeight()*0.5f-100*0.5f,iconWidth,iconWidth,iconWidth);
        batch.draw(Textures.getInstance().timeCounter,timeLimitButton.getX()-timeLimitButton.getHeight()*0.5f-100*0.5f,iconWidth,iconWidth,iconWidth);

        batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
