package com.seip.beehave;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.seip.beehave.DataStructures.SaveData;
import com.seip.beehave.DataStructures.Screens;
import com.seip.beehave.UserInterface.Screens.GameplayScreen;
import com.seip.beehave.UserInterface.Screens.LevelsScreen;
import com.seip.beehave.UserInterface.Screens.LevelsTierScreen;
import com.seip.beehave.UserInterface.Screens.MainMenuScreen;
import com.seip.beehave.UserInterface.Screens.TutorialScreen;

/**
 * Created by dillonse on 29/11/2016.
 */

public class BeehaveGame extends Game{



    public void create(){
        //load the data here
        SaveData.load();
        SaveData.tier=3;
        SaveData.level=2;
        //allocate memory of the screens
        Screens.gameplayScreen = new GameplayScreen(this,SaveData.tier,SaveData.level);
        Screens.levelsScreenEasy = new LevelsScreen(this,"Easy");
        Screens.levelsScreenMedium = new LevelsScreen(this,"Medium");
        Screens.levelsScreenHard = new LevelsScreen(this,"Hard");
        Screens.mainMenuScreen = new MainMenuScreen(this);
        Screens.levelsTierScreen = new LevelsTierScreen(this);
        Screens.tutorialScreen = new TutorialScreen(this);
        this.setScreen(Screens.mainMenuScreen);
    }

    public void render(){
        super.render();
    }

    public void dispose(){
    }
}
