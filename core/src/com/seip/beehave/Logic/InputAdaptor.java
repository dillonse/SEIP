package com.seip.beehave.Logic;

import com.badlogic.gdx.InputProcessor;
import com.seip.beehave.DataStructures.*;

/**
 * Created by dillonse on 09/11/2016.
 */

public class InputAdaptor implements InputProcessor {

    public boolean touchDown (int x, int y, int pointer, int button) {
        com.seip.beehave.DataStructures.Game.getInstance().getGameplayInput().touchInput(x,y);
        return true;
    }
    public boolean keyDown (int keycode) {
        return false;
    }

    public boolean keyUp (int keycode) {
        return false;
    }

    public boolean keyTyped (char character) {
        return false;
    }

    public boolean touchUp (int x, int y, int pointer, int button) {
        return false;
    }

    public boolean touchDragged (int x, int y, int pointer) {
        return false;
    }

    public boolean mouseMoved (int x, int y) {
        return false;
    }

    public boolean scrolled (int amount) {
        return false;
    }
}
