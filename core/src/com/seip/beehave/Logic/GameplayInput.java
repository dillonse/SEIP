package com.seip.beehave.Logic;

import com.badlogic.gdx.Gdx;
import com.seip.beehave.DataStructures.*;
import com.seip.beehave.DataStructures.Game;

/**
 * Created by dillonse on 22/11/2016.
 */

public class GameplayInput {
    public void touchInput(int i,int j){
        j = Gdx.graphics.getHeight()-j;
        Hexagon hexagon= Game.getInstance().getGrid().locate(new Point2(i,j));

        GameplayManager manager = Game.getInstance().getGameplayManager();
        manager.activateHexagon(hexagon);
        manager.checkState();
    }
}
