package com.seip.beehave.Logic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.RandomXS128;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.utils.Timer;
import com.seip.beehave.DataStructures.Game;
import com.seip.beehave.DataStructures.Hexagon;
import com.seip.beehave.DataStructures.HexagonGrid;
import com.seip.beehave.DataStructures.HexagonType;
import com.seip.beehave.DataStructures.Point2i;
import com.seip.beehave.DataStructures.SaveData;
import com.seip.beehave.DataStructures.Screens;
import com.seip.beehave.UserInterface.Colors;

import java.sql.Time;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

/**
 * Created by dillonse on 21/11/2016.
 */

public class GameplayManager {
    
    public GameplayManager(){
    }

    public void activateHexagon(Hexagon hexagon){

        GameplayMode mode= Game.getInstance().getGameplayMode();
        if(hexagon.getType()==HexagonType.Border||
                hexagon.getType()==HexagonType.ExploredVacant||
                hexagon.getType()==HexagonType.ExploredOccupied)
            return;

        if(hexagon.getType()==HexagonType.Occupied){
            hexagon.setType(HexagonType.ExploredOccupied);
            mode.setExploredOccupiedCells(mode.getExploredOccupiedCells()+1);
            Game.getInstance().getOverbar().setKills(mode.getKillLimit()-mode.getExploredOccupiedCells());
        }
        else if(hexagon.getType()==HexagonType.Vacant){
            hexagon.setType(HexagonType.ExploredVacant);
            mode.setExploredVacantCells(mode.getExploredVacantCells()+1);
        }

        revealHexagon(hexagon);
    }

    public void placeBees(){
        Random random = new Random();
        HexagonGrid grid= Game.getInstance().getGrid();
        GameplayMode mode = Game.getInstance().getGameplayMode();
        java.util.List<Short> electedElements = uniqueRandom(mode.getNumOfBees());
        for(int i=0;i<mode.getNumOfBees();i++) {
            //use a uniform distribution to pick the 2D array indices
            int element = electedElements.get(i);
            int iIndex = element/mode.getGridDimensions().y+1 ;
            int jIndex = element%mode.getGridDimensions().y+1 ;
            Hexagon luckyHex = grid.getHexagon(iIndex,jIndex);

            if(luckyHex.getType()==HexagonType.Occupied)continue;

            luckyHex.setType(HexagonType.Occupied);
            luckyHex.setRank(luckyHex.getRank()+1);
            Hexagon[] neighbours= new Hexagon[6];
            neighbours[0]=grid.getHexagon(iIndex,jIndex-1);
            neighbours[1]=grid.getHexagon(iIndex,jIndex+1);
            neighbours[2]=grid.getHexagon(iIndex-1,jIndex);
            neighbours[3]=grid.getHexagon(iIndex+1,jIndex);
            if(iIndex%2==1){
                neighbours[4]=grid.getHexagon(iIndex+1,jIndex+1);
                neighbours[5]=grid.getHexagon(iIndex-1,jIndex+1);
            }
            else{
                neighbours[4]=grid.getHexagon(iIndex+1,jIndex-1);
                neighbours[5]=grid.getHexagon(iIndex-1,jIndex-1);
            }

            for (Hexagon hex: neighbours){
                hex.setRank(hex.getRank()+1);
            }
        }

    }

    public void revealHexagons(){
        HexagonGrid grid = Game.getInstance().getGrid();
        GameplayMode mode= Game.getInstance().getGameplayMode();
        for (int i = 0; i < mode.getGridDimensions().x; i++) {
            for (int j = 0; j < mode.getGridDimensions().y; j++) {
                Hexagon hexagon = grid.getHexagon(i+1, j+1);
                revealHexagon(hexagon);
            }
        }
    }

    public void revealHexagon(Hexagon hexagon){
        if (hexagon.getType() == HexagonType.ExploredOccupied||hexagon.getType()==HexagonType.Occupied) {
            hexagon.setRenderTexture(true);
        }
        if (hexagon.getRank() > 2) {
            hexagon.setColor(Colors.exploredWarningNearColor);
        } else if (hexagon.getRank() > 0) {
            hexagon.setColor(Colors.exploredWarningFarColor);
        } else {
            hexagon.setColor(Colors.exploredColor);
        }
    }

    public void checkState(){
        GameplayMode mode = Game.getInstance().getGameplayMode();
        if(mode.checkLoseState()){
            Game.getInstance().getLoseDialog().setEnabled(true);
            Gdx.input.setInputProcessor(Game.getInstance().getLoseDialog().getStage());
            Game.getInstance().getGameplayManager().revealHexagons();
            stopClock();
            Timer.instance().clear();
        }
        else if(mode.checkWinState()){
            Game.getInstance().getWinDialog().setEnabled(true);
            Gdx.input.setInputProcessor(Game.getInstance().getWinDialog().getStage());
            Game.getInstance().getGameplayManager().revealHexagons();
            stopClock();
            Timer.instance().clear();
            if((mode.getTier()*10+mode.getLevel())>=(SaveData.tier*10+SaveData.level)) {
                if(SaveData.level==9){
                    SaveData.level=1;
                    SaveData.tier++;
                }
                else{
                    SaveData.level++;
                }
                SaveData.save();
                Screens.levelsScreenHard.refresh();
                Screens.levelsScreenMedium.refresh();
                Screens.levelsScreenEasy.refresh();
            }
        }
    }

    public void resetGame(){
        for(int i=0;i<Game.getInstance().getGameplayMode().getGridDimensions().x;i++){
            for(int j=0;j<Game.getInstance().getGameplayMode().getGridDimensions().y;j++){
                Hexagon hexagon = Game.getInstance().getGrid().getHexagon(i+1,j+1);
                hexagon.setRank(0);
                if(hexagon.getType()!=HexagonType.Border) {
                    hexagon.setType(HexagonType.Vacant);
                    hexagon.setColor(Colors.unexploredColor);
                    hexagon.setRenderTexture(false);
                }
            }
        }
        Game.getInstance().getGameplayMode().reset();
        Game.getInstance().getOverbar().setKills(Game.getInstance().getGameplayMode().getKillLimit());
        this.placeBees();
        Gdx.input.setInputProcessor(Game.getInstance().getInputAdaptor());
        Timer.instance().schedule(new Timer.Task(){
                                      @Override
                                      public void run() {
                                          clock();
                                      }
                                  }
                , 0        //    (delay)
                , 1     //    (seconds)
        );
        startClock();
    }

    private void clock(){
        Game.getInstance().getGameplayMode().updateClockperSecond();
        Game.getInstance().getOverbar().setTime(new Time(Game.getInstance().getGameplayMode().getSeconds()*1000));
        checkState();
    }

    private void startClock(){
        Timer.instance().start();
    }

    private void stopClock(){
        Timer.instance().stop();
    }

    private java.util.List<Short> uniqueRandom(int numofElements){
        //create the list of the elements
        int flattenDimension = Game.getInstance().getGameplayMode().getGridDimensions().x*Game.getInstance().getGameplayMode().getGridDimensions().y;
        LinkedList<Short> elements= new LinkedList<Short>();
        for(short i=0;i<flattenDimension;i++){
            elements.add(new Short(i));
        }
        Collections.shuffle(elements);
        return elements.subList(0,numofElements);
    }

}
