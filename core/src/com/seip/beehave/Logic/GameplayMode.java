package com.seip.beehave.Logic;

import com.seip.beehave.DataStructures.HexagonGrid;
import com.seip.beehave.DataStructures.Point2i;

/**
 * Created by dillonse on 16/11/2016.
 * This class implements the classic gameplay mode and the checkWinState and checkLoseState can be extended for new gameplay modes
 */

public class GameplayMode {

    private int tier;
    private int level;
    private int killLimit;
    private int numOfBees;
    private int exploredVacantCells;
    private int exploredOccupiedCells;
    private int numOfCells;
    private Point2i gridDimensions;

    private int seconds;
    private int secondsAmount;
    private int timeDirection=-1;

    public GameplayMode(int tier,int level){
        this.tier=tier;
        this.level=level;
        if(tier==1){
            gridDimensions=new Point2i(7,5);
            numOfBees = 5;
            switch (level){
                case 1:
                    killLimit=3;
                    secondsAmount=120;
                    break;
                case 2:
                    killLimit=4;
                    secondsAmount=90;
                    break;
                case 3:
                    secondsAmount=90;
                    killLimit=3;
                    break;
                case 4:
                    killLimit=2;
                    secondsAmount=60;
                    break;
                case 5:
                    secondsAmount=60;
                    killLimit=3;
                    break;
                case 6:
                    killLimit=2;
                    secondsAmount=60;
                    break;
                case 7:
                    killLimit=1;
                    secondsAmount=45;
                    break;
                case 8:
                    killLimit=2;
                    secondsAmount=30;
                    break;
                case 9:
                    killLimit=1;
                    secondsAmount=30;
                    break;
            }
        }
        else if(tier==2){
            gridDimensions = new Point2i(12,7);
            numOfBees = 25;
            switch (level){
                case 1:
                    killLimit=5;
                    secondsAmount=180;
                    break;
                case 2:
                    killLimit=6;
                    secondsAmount=150;
                    break;
                case 3:
                    secondsAmount=150;
                    killLimit=5;
                    break;
                case 4:
                    killLimit=4;
                    secondsAmount=150;
                    break;
                case 5:
                    secondsAmount=120;
                    killLimit=5;
                    break;
                case 6:
                    killLimit=4;
                    secondsAmount=120;
                    break;
                case 7:
                    killLimit=3;
                    secondsAmount=120;
                    break;
                case 8:
                    killLimit=4;
                    secondsAmount=90;
                    break;
                case 9:
                    killLimit=3;
                    secondsAmount=90;
                    break;
            }
        }
        else if(tier==3){
            gridDimensions =new Point2i(17,9);
            numOfBees = 40;
            switch (level){
                case 1:
                    killLimit=2;
                    secondsAmount=180;
                    break;
                case 2:
                    killLimit=3;
                    secondsAmount=150;
                    break;
                case 3:
                    secondsAmount=150;
                    killLimit=2;
                    break;
                case 4:
                    killLimit=1;
                    secondsAmount=150;
                    break;
                case 5:
                    secondsAmount=120;
                    killLimit=2;
                    break;
                case 6:
                    killLimit=1;
                    secondsAmount=120;
                    break;
                case 7:
                    killLimit=1;
                    secondsAmount=120;
                    break;
                case 8:
                    killLimit=1;
                    secondsAmount=90;
                    break;
                case 9:
                    killLimit=1;
                    secondsAmount=90;
                    break;
            }
        }
        this.numOfCells= (gridDimensions.x)*(gridDimensions.y);
        this.seconds=secondsAmount;
    }

    public boolean checkWinState(){

        return numOfCells==exploredVacantCells+numOfBees;
    }

    public boolean checkLoseState(){
        return (exploredOccupiedCells>=killLimit)||(seconds==0);
    }

    public void reset(){
        exploredOccupiedCells=0;
        exploredVacantCells=0;
        seconds=secondsAmount;

    }

    public void updateClockperSecond(){
        seconds+=timeDirection;
        if(seconds<0)seconds=0;
    }

    public int getExploredOccupiedCells() {
        return exploredOccupiedCells;
    }

    public void setExploredOccupiedCells(int exploredOccupiedCells) {
        this.exploredOccupiedCells = exploredOccupiedCells;
    }

    public int getExploredVacantCells() {
        return exploredVacantCells;
    }

    public void setExploredVacantCells(int exploredVacantCells) {
        this.exploredVacantCells = exploredVacantCells;
    }

    public Point2i getGridDimensions() {
        return gridDimensions;
    }

    public void setGridDimensions(Point2i gridDimensions) {
        this.gridDimensions = gridDimensions;
    }

    public int getNumOfBees() {
        return numOfBees;
    }

    public void setNumOfBees(int numOfBees) {
        this.numOfBees = numOfBees;
    }

    public long getSeconds() {
        return seconds;
    }

    public void setSeconds(int clock) {
        this.seconds = clock;
    }

    public int getKillLimit() {
        return killLimit;
    }

    public int getTier() {
        return tier;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setTier(int tier) {
        this.tier = tier;
    }
}
