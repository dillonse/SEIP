package com.seip.beehave.Rendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.seip.beehave.UserInterface.Colors;
import com.seip.beehave.UserInterface.Styles;

/**
 * Created by dillonse on 22/11/2016.
 */

public class DialogRenderer {
    private TextButton right,left;
    public DialogRenderer(String leftString,String rightString,Stage stage){
       Skin headerSkin = Styles.getHeaderButtonStyle();

        Image buttonsBackgroundImage = new Image(headerSkin.newDrawable("default",Colors.messageColor));
        buttonsBackgroundImage.sizeBy(Gdx.graphics.getWidth()*0.2f,Gdx.graphics.getHeight());
        buttonsBackgroundImage.setPosition(Gdx.graphics.getWidth()*0.5f-buttonsBackgroundImage.getWidth()*0.5f,0);
        stage.addActor(buttonsBackgroundImage);

        // Create a button with the "default" TextButtonStyle. A 3rd parameter can be used to specify a name other than "default".
        right = new TextButton(rightString, headerSkin,"default");
        right.setTransform(true);
        right.rotateBy(90);
        right.setPosition(Gdx.graphics.getWidth()*0.5f+right.getHeight()*0.5f,Gdx.graphics.getHeight()*0.75f-right.getWidth()*0.5f);
        stage.addActor(right);

        left = new TextButton(leftString, headerSkin,"default");
        left.setTransform(true);
        left.rotateBy(90);
        left.setPosition(Gdx.graphics.getWidth()*0.5f+left.getHeight()*0.5f,Gdx.graphics.getHeight()*0.25f-left.getWidth()*0.5f);
        stage.addActor(left);

    }
    public void render(Stage stage) {
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    public TextButton getLeft() {
        return left;
    }

    public void setLeft(TextButton left) {
        this.left = left;
    }

    public TextButton getRight() {
        return right;
    }

    public void setRight(TextButton right) {
        this.right = right;
    }

}
