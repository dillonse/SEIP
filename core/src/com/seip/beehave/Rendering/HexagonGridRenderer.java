package com.seip.beehave.Rendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.seip.beehave.DataStructures.Hexagon;
import com.seip.beehave.DataStructures.HexagonType;
import com.seip.beehave.DataStructures.Point2;
import com.seip.beehave.UserInterface.Colors;

/**
 * Created by dillonse on 21/11/2016.
 */

public class HexagonGridRenderer{
    private float scaleX;
    private float scaleY;
    private ShapeRenderer renderer;

    public HexagonGridRenderer(Hexagon[][] grid){
        scaleX =((float)Gdx.graphics.getWidth())/((grid.length)*0.75f+0.25f);
        scaleY = (Gdx.graphics.getHeight()/(grid[grid.length-1].length));

        for (int i=0;i<grid.length;i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j].initRenderer(new Point2(i*0.75f+0.25f, j+(i%2)*0.5f-0.25f),new Point2(scaleX,scaleY));
            }
        }
        //set the edge colours
        for(int i=0;i<grid.length;i++){
            grid[i][0].setColor(Colors.edgeColor);
            grid[i][0].setType(HexagonType.Border);
            grid[i][grid[i].length-1].setColor(Colors.edgeColor);
            grid[i][grid[i].length-1].setType(HexagonType.Border);
        }
        for(int i=0;i<grid[0].length;i++){
            grid[0][i].setColor(Colors.edgeColor);
            grid[0][i].setType(HexagonType.Border);
            grid[grid.length-1][i].setColor(Colors.edgeColor);
            grid[grid.length-1][i].setType(HexagonType.Border);
        }
        renderer= new ShapeRenderer();
        renderer.scale(scaleX,scaleY,1);
    }

    public void renderColours(Hexagon[][] grid){
        //render all hexagons fills
        for (int i=0;i<grid.length;i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j].renderColour();
            }
        }
        for (int i=0;i<grid.length;i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j].renderTexture();
            }
        }
    }

    public void renderOutline(Hexagon[][] grid){
        Gdx.gl.glLineWidth(5f);
        renderer.begin(ShapeRenderer.ShapeType.Line);
        for(int i=1;i<grid.length-1;i++){
            for(int j=1;j<grid[i].length-1;j++){
                Point2 center = grid[i][j].getCentre();
                renderer.translate(center.x,center.y,0);
                renderer.line(0,0.5f,0.25f,0f);
                renderer.line(0.25f,0f,0.75f,0f);
                renderer.line(0.75f,0f,1f,0.5f);
                renderer.translate(-center.x,-center.y,0);
            }
        }
        for(int i=1;i<grid[1].length-2;i++){
            Point2 center = grid[1][i].getCentre();
            renderer.translate(center.x,center.y,0);
            renderer.line(0,0.5f,0.25f,1f);
            renderer.translate(-center.x,-center.y,0);
        }
        for(int i=1;i<grid[grid.length-2].length-2;i++){
            Point2 center = grid[grid.length-2][i].getCentre();
            renderer.translate(center.x,center.y,0);
            renderer.line(1f,0.5f,0.75f,1f);
            renderer.translate(-center.x,-center.y,0);
        }
        for(int i=2;i<grid.length-1;i+=2){
            Point2 center = grid[i][grid[i].length-2].getCentre();
            renderer.translate(center.x,center.y,0);
            renderer.line(0.25f,1f,0.75f,1f);
            renderer.translate(-center.x,-center.y,0);
        }
        for(int i=1;i<grid.length-1;i+=2){
            Point2 center = grid[i][grid[i].length-2].getCentre();
            renderer.translate(center.x,center.y,0);
            renderer.line(0f,0.5f,0.25f,1f);
            renderer.line(0.25f,1f,0.75f,1f);
            renderer.line(0.75f,1f,1f,0.5f);
            renderer.translate(-center.x,-center.y,0);
        }
        if(grid.length%2==0){
            Point2 center = grid[grid.length-2][grid[grid.length-2].length-2].getCentre();
            renderer.translate(center.x,center.y,0);
            renderer.line(1f,0.5f,0.75f,1f);
            renderer.translate(-center.x,-center.y,0);
        }
        renderer.end();
    }
    /////
    public float getScaleX() {
        return scaleX;
    }

    public void setScaleX(float scaleX) {
        this.scaleX = scaleX;
    }

    public float getScaleY() {
        return scaleY;
    }

    public void setScaleY(float scaleY) {
        this.scaleY = scaleY;
    }

}
