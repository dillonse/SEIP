package com.seip.beehave.Rendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.seip.beehave.UserInterface.Styles;
import com.seip.beehave.UserInterface.Textures;

import java.sql.Time;

/**
 * Created by dillonse on 21/11/2016.
 */

public class OverbarRenderer {
    private SpriteBatch batch;
    private BitmapFont font;
    private ShapeRenderer renderer;
    private GlyphLayout layout;
    float width;
    float height;
    //timer
    float ratioTimer;
    float widthTimer;
    float heighTimer;
    String timeString;
    Matrix4 timeMatrix;
    //kill limit
    float rationKills;
    float widthKills;
    float heightKills;
    String killsString;
    Matrix4 killsMatrix;



    public OverbarRenderer(){
        renderer = new ShapeRenderer();
        width = Gdx.graphics.getWidth()*0.05f;
        height = Gdx.graphics.getHeight();

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Lobster_1.3.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = new Float(64* Styles.getScale().x).intValue();
        font = generator.generateFont(parameter);
        generator.dispose();

        batch = new SpriteBatch();
        //batch.setTransformMatrix(renderer.getTransformMatrix());
        killsMatrix = new Matrix4(batch.getTransformMatrix());
        timeMatrix = new Matrix4(batch.getTransformMatrix());
        killsMatrix.rotate(0,0,1,90);
        timeMatrix.rotate(0,0,1,90);
        killsMatrix.translate(height*0.1f,-width*0.5f,0);
        timeMatrix.translate(height*0.5f,-width*0.5f,0);

        layout = new GlyphLayout();
        layout.setText(font,"0:00");

        widthTimer = layout.width;
        heighTimer = layout.height;
    }

    public void render(){
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.setColor(com.seip.beehave.UserInterface.Colors.overbarColor);
        renderer.rect(0,0,width,Gdx.graphics.getHeight());
        renderer.end();
        batch.begin();
        font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        batch.setTransformMatrix(timeMatrix);
        font.draw(batch,timeString,-widthTimer*0.5f, +heighTimer*0.5f);
        batch.setTransformMatrix(killsMatrix);
        font.draw(batch,killsString,+width*0.5f,+heighTimer*0.5f);
        batch.draw(Textures.getInstance().casualBee,-width*0.5f,-width*0.5f,width,width);
        batch.end();
    }

    public String getTimeString() {
        return timeString;
    }

    public void setTimeString(String timeString) {
        this.timeString = timeString;
    }

    public void setKillsString(String killsString) {
        this.killsString = killsString;
    }
}
