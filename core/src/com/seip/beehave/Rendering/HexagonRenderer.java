package com.seip.beehave.Rendering;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.seip.beehave.DataStructures.Point2;
import com.seip.beehave.UserInterface.Colors;
import com.seip.beehave.UserInterface.Textures;

/**
 * Created by dillonse on 21/11/2016.
 */

public class HexagonRenderer {
    private Color color;
    private Point2 centre;
    private SpriteBatch batch;
    private ShapeRenderer renderer;
    private boolean renderTexture;

    public HexagonRenderer(Point2 position,Point2 scale){
        renderer = new ShapeRenderer();
        renderer.scale(scale.x,scale.y,1);
        renderer.translate(position.x,position.y,0);

        batch = new SpriteBatch();
        batch.setTransformMatrix(renderer.getTransformMatrix());

        this.color = Colors.unexploredColor;
        this.centre = position;
    }

    public void renderColour(){
        renderer.setColor(color);
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.triangle(0.25f,0f,0f,0.5f,0.25f,1f);
        renderer.rect(0.25f,0f,0.5f,1f);
        renderer.triangle(0.75f,0f,0.75f,1f,1f,0.5f);
        renderer.end();
    }
    public void renderTexture(){
        if(!renderTexture)return;
        batch.begin();
        batch.draw(Textures.getInstance().casualBee, 0f, 0f, 1f, 1f);
        batch.end();
    }
    //////
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Point2 getCentre() {
        return centre;
    }

    public void setCentre(Point2 centre) {
        this.centre = centre;
    }
    public boolean isRenderTexture() {
        return renderTexture;
    }

    public void setRenderTexture(boolean renderTexture) {
        this.renderTexture = renderTexture;
    }
}
