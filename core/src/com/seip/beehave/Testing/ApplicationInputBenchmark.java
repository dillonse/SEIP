package com.seip.beehave.Testing;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Timer;
import com.seip.beehave.DataStructures.Game;
import com.seip.beehave.DataStructures.Point2;
import com.seip.beehave.DataStructures.Point2i;
import com.seip.beehave.Logic.GameplayInput;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by dillonse on 25/11/2016.
 * This class is responsible for benchmarking
 */

public class ApplicationInputBenchmark {

    GameplayInput input;
    int counter=0;
    public  void benchmark(int numOfTrials,float period){
        Random random = new Random();

        final ArrayList<Point2i> testPoints= new ArrayList<Point2i>(numOfTrials);

        for(int i=0;i<numOfTrials;i++){
            int indexI=random.nextInt(Gdx.graphics.getWidth());
            int indexJ=random.nextInt(Gdx.graphics.getHeight());
            testPoints.add(new Point2i(indexI,indexJ));
        }

        input = Game.getInstance().getGameplayInput();
        counter=0;
        Timer.schedule(new Timer.Task(){
                           @Override
                           public void run() {
                               Point2i touch = testPoints.get(counter++);
                               hit(touch.x,touch.y);
                           }
                       }
                , 0        //    (delay)
                , period     //    (interval)
                ,numOfTrials-1 // (repeats)
        );

    }

    private void hit(int i,int j){
        input.touchInput(i,j);
    }
}
